<?php
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

return [
    'production' => false,
    'baseUrl' => '',
    'collections' => [
        'cameras' => [
            'items' => function() {
                $directus_token = getenv('DIRECTUS_TOKEN');
                $camerasApiUrl = 'https://directus2.bob-humphrey.com/_/items/cameras?access_token=' 
                    . $directus_token 
                    . '&fields=sort,description,price,for_sale,image.data.*,'
                    . 'image_creator,image_creator_url&sort=sort&status=published';
                $data = json_decode(file_get_contents($camerasApiUrl));
                $cameras = collect($data->data)->map(function ($camera) {
                    return [
                        'description' => $camera->description,
                        'price' => $camera->price,
                        'for_sale' => $camera->for_sale,
                        'image' => $camera->image->data->thumbnails[1]->url,
                        'image_creator' => $camera->image_creator,
                        'image_creator_url' => $camera->image_creator_url,
                    ];
                });
                return $cameras;  
            },
        ],
    ],
];

