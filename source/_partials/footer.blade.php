<div class="bg-grey-500">
    <div class="flex justify-center w-full py-10">
        <a href="https://bob-humphrey.com">
            <img class="w-16" src="/assets/images/bh-logo.gif" alt="Bob Humphrey website ">
        </a>
    </div>
</div>