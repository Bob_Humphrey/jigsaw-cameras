<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="A fictitious vintage camera shop, built to demonstrate
        how a website is put together using the Directus content management system and 
        the Jigsaw static site builder.">
        <title>
            Vintage Camera Shop
        </title>
        <link rel="stylesheet" href="{{ mix('css/main.css', 'assets/build') }}">
        <!-- Matomo -->
        <script type="text/javascript">
        var _paq = window._paq || [];
        /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function() {
            var u="//analytics.bob-humphrey.com/";
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', '1']);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
        })();
        </script>
        <!-- End Matomo Code -->
    </head>
    <body>
 
        <nav class="lg:w-full">
            @include('_partials.header')
        </nav>

        <main class="lg:w-full">
            @yield('content')
        </main>
        
        <footer class="lg:w-full">
            @include('_partials.footer')
        </footer>

    </body>
</html>

