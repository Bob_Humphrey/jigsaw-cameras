@extends('_layouts.master')

@php

@endphp

@section('content')
<div class="flex justify-center w-full mt-10 mb-6">
    <div class="w-11/12 lg:w-2/3">
        <div class="flex flex-wrap -mx-2">

            @foreach ($cameras as $camera)

            <div class="lg:w-1/3 px-2 mb-4 flex items-stretch">
                <div class="flex flex-col bg-grey-200 text-grey-700 rounded-lg text-center">
                    <img class="rounded-t-lg" src="{{ $camera->image }}" alt="{{ $camera->description }} photo">
                    <div class="pt-4">
                        <div class="font-serif ">
                            {{ $camera->description }}
                        </div>
                        <div class="font-serif">
                            ${{ $camera->price }}
                        </div>
                        <div class="text-xs text-grey-600 mb-4"> 
                            <a href="{{ $camera->image_creator_url }}"
                            target="_blank"
                            rel="noopener noreferrer">
                                Photo credit: {{ $camera->image_creator }}
                            </a>
                        </div>
                        @if ( $camera->for_sale )
                            <div class="font-serif bg-grey-600 text-white py-3 rounded-b-lg cursor-pointer">
                                Add to Cart
                            </div>
                        @else
                            <div class="font-serif py-3">
                                Sold
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            @endforeach

        </div>
    </div>
</div>
@endsection